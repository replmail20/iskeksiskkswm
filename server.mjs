import { WebSocketServer } from 'ws';

const wss = new WebSocketServer({ port: 8080 });

wss.on('connection', function connection(ws) {
  console.log('A new client connected');
  
  ws.on('message', function message(data) {
    console.log('Received:', data);
    ws.send(`Hello, you sent -> ${data}`);
  });

  ws.on('close', () => {
    console.log('Client has disconnected');
  });
});
